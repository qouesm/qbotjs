import { Client, MessageEmbed } from 'discord.js';
const client = new Client();
// import token from './token.js';

const prefix = '.';

const commands = {
    'help': 'static',
    'ping': 'static',
    'hi': 'static',
    'time': 'static',
    'avatar': 'static',
    'prunemsg': 'static',
    'imposter': 'static',
    'onevote': 'static',
    'ov': 'static',
    'ovc': 'static',
    'color': 'static',
    'c': 'static',
    'mute': 'static',
    'm': 'static',
    'unmute': 'static',
    'umute': 'static',
    'um': 'static',
    'togglemute': 'static',
    'tmute': 'static',
    'tm': 'static',
    'panic': 'async',
    'stfu': 'async',
    'stinky': 'async',
    'uhohstinky': 'async',
    'uhoh': 'async',
    'bazinga': 'async',
    'gnome': 'async',
    'oo': 'async',
    'oopaul': 'async',
    'oocave': 'async',
    'chum': 'async',
    'diarrhea': 'async',
    'scatman': 'async',
    'eat': 'async',
    'speak': 'async',
    'ok': 'async',
    'okay': 'async',
    'bup': 'async',
    'bupvoid': 'async',
    'ass': 'async'
};

const qbotID = '762120681415507979';
const qouesmID = '130388483494641664';
const tylerID = '171425139328548865';
const noahID = '397324502436020239';
const dylanID = '231924959671353344';
const charlieID = '132581846566436864';
const maxID = '192626935094968321';

const qHandlers = [
    qouesmID,
    tylerID,
    noahID,
    dylanID,
    charlieID
]

const thinkingSentences = [
    'hmmm',
    'im thinking',
    'im consulting Willow',
    'im asking Andrew',
    'lets see here',
    'well',
    'i need a minute',
    'perhaps',
    'uh',
    'im shaking my magic 8 ball'
]

const confirmationSentences = [
    'it has to be',
    'i can feel it in my *bones*',
    'i think',
    'yup',
    'for sure',
    'im positive',
    'if not, then vote me out',
    'i can smell it',
    'i saw them vent',
    'they vented',
    'they faked keys',
    'they self reported',
    'i was checking vitals',
    'i was doing wires'
]

const auColors = [
    'pink',
    'red',
    'orange',
    'yellow',
    'light green', 'lime',
    'green',
    'light blue', 'cyan', 'teal',
    'blue',
    'purple',
    'brown',
    'black',
    'white'
]

const randomThinkingSentence = () => {
    return thinkingSentences[Math.floor((Math.random() * thinkingSentences.length))] + '...';
};

const randomConfirmationSentence = () => {
    return confirmationSentences[Math.floor((Math.random() * thinkingSentences.length))];
};

const playMP3 = async (message, file, vol, time) => {
    message.delete();
    if (!isQAdmin(message)) {
        message.reply('not qhandler');
        return;
    }
    if (vol <= 0 || vol > 1) {
        message.reply('invalid volume');
        return;
    }
    const vChannel = message.member.voice.channel;
    let connection;
    let dispatcher;
    if (vChannel) {
        connection = await vChannel.join();
    } else {
        message.reply('join a voice channel');
        return;
    }
    console.log(`${message.author.username} is playing ${file}`);
    if (!vol) { vol = 1 }
    dispatcher = connection.play(`./mp3/${file}.mp3`, { volume: vol });

    dispatcher.on('start', () => {
        console.log('playing audio');
        if (file !== 'stfu') {
            message.channel.send({
                files: [{ attachment: `./mp3/${file}.png`, }]
            }).then((msg) => {
                msg.delete({ timeout: time });
            });
        } else {
            message.channel.send({
                files: [{ attachment: `./mp3/${file}.gif`, }]
            }).then((msg) => {
                msg.delete({ timeout: time });
            });
        }
    });
    dispatcher.on('finish', () => {
        console.log('stopped audio');
        vChannel.leave();
    });
    dispatcher.on('error', console.error);
}

const isQAdmin = (message) => {
    return qHandlers.includes(message.author.id) || message.member.roles.cache.some(role => role.name === 'qhandler');
};

const help = (message) => {
    let staticCommands = [];
    let asyncCommands = [];
    for (const [key, value] of Object.entries(commands)) {
        value === 'static' ? staticCommands.push(key) : asyncCommands.push(key);
    }
    const embed = new MessageEmbed()
        .setColor('ff4444')
        .setDescription('https://gitlab.com/qouesm/qbot')
        .setAuthor('qbot')
        .setTitle(`Prefix: \`${prefix}\``)
        .setThumbnail(client.user.avatarURL())
        .addFields(
            { name: 'Commands', value: staticCommands, inline: true },
            { name: 'Audio', value: asyncCommands, inline: true },
        )
        .setFooter(`@${client.users.cache.get(qouesmID).tag}`);
    message.channel.send(embed);

}

let ovUser;

client.once('ready', () => {
    // client.user.setActivity('@qouesm')
    client.user.setActivity('@qouesm', { type: 'WATCHING' })
        .then(presence => console.log(`activity set to ${presence.activities[0].name}`))
        .catch(console.error);
    console.log(getTimestamp() + 'Ready');

    ovUser = client.users.cache.get(qbotID);
});

const getTimestamp = () => {
    const d = new Date();

    let hours = d.getHours().toString();
    let minutes = d.getMinutes().toString();
    let seconds = d.getSeconds().toString();

    hours = hours.length === 1 ? '0' + hours : hours;
    minutes = minutes.length === 1 ? '0' + minutes : minutes;
    seconds = seconds.length === 1 ? '0' + seconds : seconds;

    return `[${hours}:${minutes}:${seconds}] `;
};

client.on('message', message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    const args = message.content.slice(prefix.length).trim().split(' ');
    const command = args.shift().toLowerCase();
    const vChannel = message.member.voice.channel;
    console.log();
    console.log(message.author.tag);
    console.log(command);
    console.log(args);
    if (command in commands) {
        if (commands[command] !== 'static') { return; }
    }
    // else {
    //     message.delete();
    //     message.reply('bad command').then((logMsg) => {
    //         logMsg.delete({ timeout: 2500 });
    //     });
    //     return;
    // }

    switch (command) {
        case 'help':
            help(message);
            break;
        case 'ping':
            message.channel.send('pong');
            break;
        case 'hi':
            switch (message.author.id) {
                case qouesmID:
                    message.reply('hi qt');
                    break;
                case tylerID:
                    message.reply('shhh');
                    break;
                case noahID:
                    message.reply('hi gamer');
                    break;
                case dylanID:
                    message.reply('hi chad');
                    break;
                case charlieID:
                    message.reply('you are not egirl');
                    break;
                case maxID:
                    message.reply('stop talking about your dick');
                    break;
                default:
                    message.reply('who are you');
                    break;
            }
            break;
        case 'time':
            message.channel.send(getTimestamp());
            break;
        case 'avatar':
            const user = message.mentions.users.first();
            if (message.mentions.users.size > 1) {
                message.channel.send('too many users');
            } else if (user) {
                message.channel.send(user.displayAvatarURL({ format: "png", dynamic: true }));
            } else {
                message.channel.send(message.author.displayAvatarURL({ format: "png", dynamic: true }));
            }
            break;
        case 'prunemsg':
            if (!isQAdmin(message)) {
                message.reply('not qhandler').then((logMsg) => {
                    logMsg.delete({ timeout: 10000 });
                });
                break;
            }
            const count = parseInt(args[0]);
            const minPrune = 2;
            const maxPrune = 100;
            message.delete();
            if (isNaN(count)) {
                message.reply('prunemsg: bad argument').then((logMsg) => {
                    logMsg.delete({ timeout: 10000 });
                });
            } else if (count < minPrune || count > maxPrune) {
                message.reply(`prumemsg: ${minPrune} < messages < ${maxPrune}`).then((logMsg) => {
                    logMsg.delete({ timeout: 10000 });
                });
            } else {
                message.channel.bulkDelete(count).then(() => {
                    message.reply(`pruned ${count} messages`).then((logMsg) => {
                        logMsg.delete({ timeout: 10000 });
                    });
                }).catch(err => {
                    console.error(err);
                    message.channel.send('prunemsg: some messages may be older than 2 weeks');
                });
            }
            break;
        case 'imposter':
            if (vChannel) {
                const vMembers = vChannel.members;
                const vMembersUnmute = vMembers.filter(vMembersUnmute => !vMembersUnmute.voice.serverMute);
                const vMemberRandom = vMembersUnmute.random();
                message.channel.send(randomThinkingSentence()).then((logMsg) => {
                    logMsg.delete({ timeout: Math.floor((Math.random() * 10000) + 5000) }).then(() => {
                        message.channel.send(`its ${vMemberRandom.toString()}, ${randomConfirmationSentence()}`);
                    });
                });
            } else {
                message.reply('join a voice channel');
            }
            break;
        case 'onevote':
        case 'ov':
            message.delete();
            if (vChannel) {
                const vMembers = vChannel.members;
                const vMembersUnmute = vMembers.filter(vMembersUnmute => !vMembersUnmute.voice.serverMute);
                const vMemberRandom = vMembersUnmute.random();
                // const vMemberRandom = client.users.cache.get(qouesmID);
                if (ovUser !== client.users.cache.get(qbotID)) {
                    message.reply(`onevote is currently in progress (waiting on ${ovUser.toString()} to type \`.color [color]\`)`).then((msg) => {
                        msg.delete({ timeout: 10000 });
                    });
                    break;
                }
                ovUser = vMemberRandom;
                message.channel.send(`${ovUser.toString()}, pick a color \`.color [color]\``).then((msg) => {
                    msg.delete({ timeout: 30000 });
                });
            } else {
                message.reply('join a voice channel');
            }
            break;
        case 'color':
        case 'c':
            message.delete();
            if (ovUser.id === qbotID) {
                message.reply('onevote is not running').then((msg) => {
                    msg.delete({ timeout: 10000 });
                });
                break;
            }
            if (message.author === ovUser) {
                if (auColors.indexOf(args[0]) > -1) {
                    message.channel.send(`${ovUser.toString()} voted ${args[0]}`);
                    ovUser = client.users.cache.get(qbotID);
                } else {
                    message.channel.send('invalid color');
                }
            } else {
                message.reply('you were not chosen');
            }
            break;
        case 'onevotecancel':
        case 'ovc':
            message.delete();
            if (isQAdmin) {
                message.reply('onevote cancelled');
                ovUser = client.users.cache.get(qbotID);
            } else {
                message.reply('not qhandler');
            }
            break;
        case 'mute':
        case 'm':
            if (!isQAdmin(message)) {
                message.reply('not qhandler');
                break;
            }
            if (vChannel) {
                let vMembers = vChannel.members;
                for (let member of vMembers) {
                    member[1].voice.setMute(true, 'called .mute').catch((err) => {
                        console.err(err);
                        message.reply('sorry something went wrong');
                    });
                }
                message.reply('muted');
            } else {
                message.reply('join a voice channel');
            }
            break;
        case 'unmute':
        case 'umute':
        case 'um':
            if (!isQAdmin(message)) {
                message.reply('not qhandler');
                break;
            }
            if (vChannel) {
                let vMembers = vChannel.members;
                for (let member of vMembers) {
                    member[1].voice.setMute(false, 'called .unmute').catch((err) => {
                        console.err(err);
                        message.reply('sorry something went wrong');
                    });
                }
                message.reply('unmuted');
            } else {
                message.reply('join a voice channel');
            }
            break;
        case 'togglemute':
        case 'tmute':
        case 'tm':
            if (!isQAdmin(message)) {
                message.reply('not qhandler');
                break;
            }
            if (vChannel) {
                let vMembers = vChannel.members;
                for (let member of vMembers) {
                    member[1].voice.setMute(!member[1].voice.serverMute, 'called .togglemute').catch((err) => {
                        console.err(err);
                        message.reply('sorry something went wrong');
                    });
                }
                message.reply('mute toggled');
            } else {
                message.reply('join a voice channel');
            }
            break;
    };
});

client.on('message', async message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;
    const args = message.content.slice(prefix.length).trim().split(' ');
    const command = args.shift().toLowerCase();
    // const vChannel = message.member.voice.channel;
    let vol = args[0];
    if (command in commands) {
        if (commands[command] !== 'async') { return; }
    }

    switch (command) {
        case 'panic':
            if (!vol) { vol = 0.5; }
            playMP3(message, 'panic', vol, 1750);
            break;
        case 'stfu':
            playMP3(message, 'stfu', vol, 2000);
            break;
        case 'stinky':
        case 'uhohstinky':
            playMP3(message, 'uhohstinky', vol, 4000);
            break;
        case 'uhoh':
            playMP3(message, 'uhoh', vol, 1000);
            break;
        case 'bazinga':
            playMP3(message, 'bazinga', vol, 500);
            break;
        case 'gnome':
        case 'oo':
            playMP3(message, 'oo', vol, 500);
            break;
        case 'oopaul':
            playMP3(message, 'oopaul', vol, 2250);
            break;
        case 'oocave':
            playMP3(message, 'oocave', vol, 2000);
            break;
        case 'chum':
            playMP3(message, 'chum', vol, 3000);
            break;
        case 'diarrhea':
            playMP3(message, 'diarrhea', vol, 750);
            break;
        case 'scatman':
            playMP3(message, 'scatman', vol, 9500);
            break;
        case 'eat':
            playMP3(message, 'eat', vol, 2000);
            break;
        case 'speak':
            playMP3(message, 'speak', vol, 2250);
            break;
        case 'ok':
        case 'okay':
            playMP3(message, 'ok', vol, 2000);
            break;
        case 'bup':
            playMP3(message, 'bup', vol, 500);
            break;
        case 'bupvoid':
            playMP3(message, 'bupvoid', vol, 7500);
            break;
        case 'ass':
            playMP3(message, 'ass', 0.5, 2500)
    }
});

// client.login(token);
client.login(process.env.BOT_TOKEN);
